
var idBoard = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' ],
chess = document.querySelector('.chess'),
cage = 8;
point = {
	x: 0,
	y: idBoard[0]
},
piece = {
	white: '/img/white__',
	black: '/img/black__',
	pawn: 'pawn',
	name: ['castle', 'knight', 'bishop', 'queen', 'king', 'bishop', 'knight', 'castle']
};
// колонка с цифрами
var chessNumbers = document.createElement('div');
chess.appendChild(chessNumbers);
chessNumbers.classList.add('chess__numbers');
for ( var i = idBoard.length ; i > 0; i--) {
	var chessId = document.createElement('div');
	chessNumbers.appendChild(chessId);
	chessId.classList.add('id');
	chessId.innerHTML = i;
	chessId.style.borderRight = 'none';
	chessId.style.borderBottom = 'none';
	if (i == 1) {
		chessId.style.borderBottom = '1px solid black';
	}
}
// строка с буквами
var chessLetters = document.createElement('div');
chess.appendChild(chessLetters);
chessLetters.classList.add('chess__letters');
for ( var i = 1 ; i <= idBoard.length; i++) {
	var chessId = document.createElement('div');
	chessLetters.appendChild(chessId);
	chessId.classList.add('id');
	chessId.innerHTML = idBoard[i-1];
	chessId.style.borderTop = 'none';
	chessId.style.borderRight = 'none';
	if (i == 8) {
		chessId.style.borderRight= '1px solid black';
	}
}
// поле
var board = document.createElement('div');
chess.appendChild(board);
board.classList.add('board');
// линии 
for ( var i = cage ; i > 0; i--) {
	var boardRow = document.createElement('div');
	board.appendChild(boardRow);
	boardRow.classList.add('board__row');
	point.x = i;
	boardRow.id = "row"+point.x;
	// клетки
	for ( var j = 0 ; j < cage; j++) {
		point.x = i;
		point.y = idBoard[j];
		var boardCell = document.createElement('div');
		boardRow.appendChild(boardCell);
		boardCell.classList.add('board__cell');
		boardCell.id = point.y+point.x;
		boardCell.addEventListener("click", boardCellId);
		//фигуры
		if (i == 1 || i == 2 || i == 7 || i == 8){
			var pieceRank = document.createElement('div');
			boardCell.appendChild(pieceRank);
			pieceRank.classList.add('piece');
			if (i == 1 || i == 2) {
				pieceColor = piece.white;
				if (i == 1 ) {
					pieceRank.style.backgroundImage = 'url('+pieceColor+piece.name[j]+'.png)';
					pieceRank.classList.add(piece.name[j]);
				}
				if (i == 2 ) {
					pieceRank.style.backgroundImage = 'url('+pieceColor+piece.pawn+'.png)';
					pieceRank.classList.add(piece.pawn);
				}
			}
			else {
				pieceColor = piece.black;
				if (i == 7 ) {
					pieceRank.style.backgroundImage = 'url('+pieceColor+piece.pawn+'.png)';
					pieceRank.classList.add(piece.pawn);
				}
				if (i == 8 ) {
					pieceRank.style.backgroundImage = 'url('+pieceColor+piece.name[j]+'.png)';
					pieceRank.classList.add(piece.name[j]);

				}
			}
		}
		
		pieceRank.addEventListener("click", pieceRankClass);
		

	}
}
function boardCellId() {
	console.log(this.id);
}

function pieceRankClass() {
	console.log(this.getAttribute('class'));
}
